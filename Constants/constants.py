from telegram.ext import ConversationHandler

# default accounts for first time use
DEFAULT_ACCOUNTS = {
    "کاری": 0.4,
    "مخارج": 0.3,
    "خیریه": 0.03,
    "پس‌انداز ملک": 0.07,
    "تفریح": 0.03,
    "سرمایه‌گذاری": 0.17,
}

# constants for conversation states
CALCULATOR = map(chr, range(1))
ADD_ACC_NAME, ADD_ACC_PERCENT = map(chr, range(1, 3))
EDIT_ACC, EDIT_ACC_NAME, EDIT_ACC_PERCENT = map(chr, range(3, 6))

END = ConversationHandler.END