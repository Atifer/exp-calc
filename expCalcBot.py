# main file
# run this file to run the bot

from telegram.ext import (
    Updater,
    CommandHandler,
    MessageHandler,
    Filters,
    ConversationHandler,
    CallbackQueryHandler,
)
from mongoConnection import user_collection
from start import start
import Calculator.calculator as calculator
from Constants import constants as const
from Accounting import show, add, edit, reset
import requests
import logging
import settings
import sys

# Logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)

END = ConversationHandler.END

###############################################################################

# token and telegram connection
def main():
    updater = Updater(token=settings.TOKEN, use_context=True ,request_kwargs=settings.PROXY)
    dispatch = updater.dispatcher
    
    add_conv_handler = ConversationHandler(
        entry_points=[CallbackQueryHandler(add.add_account, pattern="^addAccount$")],
        states={
            const.ADD_ACC_NAME: [MessageHandler(Filters.text, add.add_account_name)],
            const.ADD_ACC_PERCENT: [
                MessageHandler(Filters.regex(r"[0-9۰-۹]"), add.add_account_percent)
            ],
        },
        fallbacks=[CallbackQueryHandler(add.cancel_accounting, pattern="^cancel$")],
        map_to_parent={const.CALCULATOR: const.CALCULATOR, END: const.CALCULATOR},
    )

    edit_conv_handler = ConversationHandler(
        entry_points=[CallbackQueryHandler(edit.choose_edit_account, pattern="^editAccount$")],
        states={
            const.EDIT_ACC: [MessageHandler(Filters.text, edit.edit_account)],
            const.EDIT_ACC_NAME: [MessageHandler(Filters.text, edit.edit_account_name)],
            const.EDIT_ACC_PERCENT: [
                MessageHandler(Filters.regex(r"[0-9۰-۹]"), edit.edit_account_percent),
                CallbackQueryHandler(edit.edited_account_name, pattern="^editedName$"),
            ],
        },
        fallbacks=[CallbackQueryHandler(edit.cancel_accounting, pattern="^cancel$")],
        map_to_parent={const.CALCULATOR: const.CALCULATOR, END: const.CALCULATOR},
    )

    calc_conv_handler = ConversationHandler(
        entry_points=[CommandHandler("start", start)],
        states={
            const.CALCULATOR: [
                add_conv_handler,
                edit_conv_handler,
                MessageHandler(Filters.regex(r"[0-9۰-۹]"), calculator.get_check),
                MessageHandler(Filters.regex(r"\D"),show.handle_non_numeric),
            ]
        },
        fallbacks=[CommandHandler("stop", calculator.calc_stop)],
        allow_reentry=True,
    )

    dispatch.add_handler(calc_conv_handler)
    dispatch.add_handler(CallbackQueryHandler(show.call_accounts_query, pattern="^acc$"))
    dispatch.add_handler(add_conv_handler)
    dispatch.add_handler(edit_conv_handler)
    dispatch.add_handler(
        CallbackQueryHandler(reset.delete_all_accounts, pattern="^deleteAccounts$")
    )
    dispatch.add_handler(
        CallbackQueryHandler(edit.personalization, pattern="^personalize$")
    )

    updater.start_polling()
    updater.idle()

if __name__ == "__main__":
    main()