# for going back to the default_accounts

from mongoConnection import user_collection
from Accounting import show
import sys

# when user wants to go back to default accounts
def delete_all_accounts(update, context):
    query = update.callback_query
    query.answer()
    if user_collection.find_one({"_id": query.message.chat_id}).get("accounts") == {}:
        query.message.reply_text("حساب‌های شما در حالت پیشفرض قرار دارد.")
    else:
        try:
            user_collection.update_one(
                {"_id": query.message.chat_id}, {"$set": {"accounts": {}}}
            )
            query.message.reply_text("حساب‌های شما به حالت پیشفرض بازگشت.")
            show.accounts(update, context)
        except:
            print("Unexpected error on user accounts delete:", sys.exc_info()[0])