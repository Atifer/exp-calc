# module to edit a user defined account

from telegram import (
    InlineKeyboardButton,
    InlineKeyboardMarkup,
    ReplyKeyboardMarkup,
    ReplyKeyboardRemove,
)
from mongoConnection import user_collection
from Constants import constants as const
from Accounting import show
import sys

# initial step to choose the account to edit
def choose_edit_account(update, context):
    user_accounts = user_collection.find_one(update.effective_user.id).get("accounts")

    query = update.callback_query
    query.answer()

    if user_accounts:
        account_keys = [
            [str(x) + "  %" + str(show.pretty_number(round(y * 100, 2)))]
            for (x, y) in user_accounts.items()
        ]
        accounts_reply_keyboard = ReplyKeyboardMarkup(
            account_keys, one_time_keyboard=True
        )
        query.message.reply_text(
            "لطفا حسابی که می‌خواهید تغییر دهید را انتخاب کنید.",
            reply_markup=accounts_reply_keyboard,
        )
        return const.EDIT_ACC


def edit_account(update, context):
    user_accounts = user_collection.find_one(update.effective_user.id).get("accounts")

    choosed_account = update.message.text.split("  %")

    edit_account_button = [
        [
            InlineKeyboardButton("بیخیال", callback_data="cancel"),
        ]
    ]
    edit_account_markup = InlineKeyboardMarkup(edit_account_button)

    choosed_account_name = "  %".join(choosed_account[0:-1])

    if choosed_account_name in user_accounts.keys():
        if float(choosed_account[-1]) / 100 == user_accounts[choosed_account_name]:
            context.user_data["editAccountName"] = choosed_account_name
            context.user_data["editAccountPercent"] = user_accounts[
                choosed_account_name
            ]
            update.message.reply_text(
                "شما حساب '{}' را برای تغییر انتخاب کردید.".format(update.message.text),
                reply_markup=ReplyKeyboardRemove(),
            )
            update.message.reply_text(
                "اسم جدید این حساب را برای من بفرستید. ",
                reply_markup=edit_account_markup,
            )
            return const.EDIT_ACC_NAME

    return const.END


def edit_account_name(update, context):
    if len(update.message.text) > 31:
        update.message.reply_text(
            "اسمی که برای حساب جدید انتخاب کردید طولانی است،"
            " لطفا یک اسم کوتاه‌تر انتخاب کنید."
        )
        return const.EDIT_ACC_NAME
    user_accounts = user_collection.find_one(update.effective_user.id).get("accounts")

    if user_accounts.get(context.user_data["editAccountName"], None):
        del user_accounts[context.user_data["editAccountName"]]
        user_collection.update_one(
            {"_id": update.effective_user.id}, {"$set": {"accounts": user_accounts}}
        )

    edit_account_name_button = [
        [
            InlineKeyboardButton("پایان", callback_data="editedName"),
        ]
    ]
    edit_account_name_markup = InlineKeyboardMarkup(edit_account_name_button)

    name = update.message.text
    if name not in user_accounts:
        if name != context.user_data["editAccountName"]:
            context.user_data["editAccountName"] = name
            update.message.reply_text("اسم این حساب به '{}' تغییر کرد.".format(name))
            update.message.reply_text(
                "حال برای تغییر درصد حساب، درصد جدید را بصورت یک عدد ارسال کنید."
                "در غیر این صورت گزینه‌ی 'پایان' را فشار دهید.",
                reply_markup=edit_account_name_markup,
            )
            return const.EDIT_ACC_PERCENT
        else:
            update.message.reply_text(
                "اسم این حساب تغییری نکرد."
                "حال برای تغییر درصد حساب، درصد جدید را بصورت یک عدد ارسال کنید."
                "در غیر این صورت گزینه‌ی 'پایان' را فشار دهید.",
                reply_markup=edit_account_name_markup,
            )
            return const.EDIT_ACC_PERCENT
    else:
        update.message.reply_text(
            "اسم وارد شده برای حساب، تکراری است. لطفا یک اسم دیگر انتخاب کنید."
        )
        return const.EDIT_ACC_NAME


def edited_account_name(update, context):
    user_accounts = user_collection.find_one(update.effective_user.id).get("accounts")
    query = update.callback_query
    query.answer()

    user_accounts[context.user_data["editAccountName"]] = context.user_data[
        "editAccountPercent"
    ]

    try:
        user_collection.update_one(
            {"_id": update.effective_user.id}, {"$set": {"accounts": user_accounts}}
        )
    except:
        print("Unexpected error on editing user account:", sys.exc_info()[0])

    show.accounts(update, context)

    return const.END


def edit_account_percent(update, context):
    user_accounts = user_collection.find_one(update.effective_user.id).get("accounts")

    percent = float(update.message.text)

    if percent > 100 or percent < 1:
        update.message.reply_text("درصد حساب باید یک عدد بدون اعشار بین 1 تا 100 باشد.")
        return const.EDIT_ACC_PERCENT

    if sum(user_accounts.values()) + percent / 100 > 1:
        update.message.reply_text("درصد اختصاص یافته به حساب‌ها بیشتر از 100 می‌شود!")
        return const.EDIT_ACC_PERCENT

    user_accounts[context.user_data["editAccountName"]] = percent / 100

    try:
        user_collection.update_one(
            {"_id": update.effective_user.id}, {"$set": {"accounts": user_accounts}}
        )
    except:
        print("Unexpected error on editing user account:", sys.exc_info()[0])

    context.user_data.clear

    show.accounts(update, context)

    return const.END


def personalization(update, context):
    query = update.callback_query
    query.answer()

    try:
        user_collection.update_one(
            {"_id": update.effective_user.id},
            {"$set": {"accounts": const.DEFAULT_ACCOUNTS}},
        )
    except:
        print("Unexpected error on editing user account:", sys.exc_info()[0])

    context.user_data.clear
    context.bot.send_message(update.effective_user.id ,
        "حساب‌های پیشفرض به عنوان حساب‌های شما قرار گرفتند."
        " حال می‌توانید با استفاده از آن‌‌ها،"
        " مبالغ دریافتی را تقسیم‌بندی کرده یا این حساب‌ها را تغییر دهید."
    )
    show.accounts(update, context)


def cancel_accounting(update, context):
    update.callback_query.answer()
    show.accounts(update, context)
    return const.END
