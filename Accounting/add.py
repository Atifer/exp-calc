# module for adding user defined accounts

from telegram import (
    InlineKeyboardButton,
    InlineKeyboardMarkup,
)
from mongoConnection import user_collection
from Constants import constants as const
from Accounting import show
import sys

# the initial step of adding a new account
def add_account(update, context):
    user_accounts = user_collection.find_one(update.effective_user.id).get("accounts")
    context.user_data.clear()
    query = update.callback_query
    query.answer()
    cancel_accounting_button = [
        [InlineKeyboardButton("بیخیال", callback_data="cancel")]
    ]
    cancel_accounting_markup = InlineKeyboardMarkup(cancel_accounting_button)
    if user_accounts:
        if sum(user_accounts.values()) < 1:
            if len(user_accounts.keys()) < 20:
                query.message.reply_text(
                    "برای اضافه کردن حساب جدید اول اسم اون حساب رو برای من بفرستید لطفا."
                    "\nمثال: ملی، مخارج، پس انداز ملک",
                    reply_markup=cancel_accounting_markup,
                )
            else:
                query.message.reply_text(
                    "تعداد حساب‌هایتان به ۲۰ رسیده است برای همین نمی‌توانید حساب دیگری اضافه کنید، "
                    " اما می‌تونید از گزینه‌ی تغییر حساب‌های موجود استفاده کنید."
                )
                return const.CALCULATOR

        else:
            query.message.reply_text(
                "درصد حساب‌های شما به 100 رسیده، برای همین نمی‌توانید حساب جدید اضافه کنید."
                " اما می‌توانید از گزینه‌ی 'تغییر حساب‌ها' استفاده کنید."
            )
            return const.CALCULATOR
    else:
        query.message.reply_text(
            "برای اضافه کردن حساب جدید اول اسم اون حساب رو برای من بفرستید لطفا."
            "\nمثال: ملی، مخارج، پس انداز ملک",
            reply_markup=cancel_accounting_markup,
        )

    return const.ADD_ACC_NAME


def add_account_name(update, context):
    if len(update.message.text) > 31:
        update.message.reply_text(
            "اسمی که برای حساب جدید انتخاب کردید طولانی است،"
            " لطفا یک اسم کوتاه‌تر انتخاب کنید."
        )
        return const.ADD_ACC_NAME

    context.user_data["newAccountName"] = update.message.text
    ask_percent_text = """شما برای حساب جدیدتون اسم {} رو انتخاب کردید. حالا با یک عدد بین 0 و 100 بهم بگید چه درصدی از مبلغ کل باید به این حساب تعلق بگیره.
    مثال: 54""".format(
        context.user_data["newAccountName"]
    )
    update.message.reply_text(ask_percent_text)

    return const.ADD_ACC_PERCENT


def add_account_percent(update, context):
    user_accounts = user_collection.find_one(update.effective_user.id).get("accounts")
    context.user_data["newAccountPercent"] = float(update.message.text)
    percent = context.user_data["newAccountPercent"]
    if percent > 100 or percent < 1:
        update.message.reply_text("درصد حساب باید یک عدد بدون اعشار بین 1 تا 100 باشد.")
        return const.ADD_ACC_PERCENT
    if sum(user_accounts.values()) + percent / 100 > 1:
        update.message.reply_text("درصد اختصاص یافته به حساب‌ها بیشتر از 100 می‌شود!")
        return const.ADD_ACC_PERCENT
    user_accounts[context.user_data["newAccountName"]] = (
        float(context.user_data["newAccountPercent"]) / 100
    )
    try:
        user_collection.update_one(
            {"_id": update.message.chat_id}, {"$set": {"accounts": user_accounts}}
        )
    except KeyError:
        print("Unexpected error on editing user account:", sys.exc_info()[0])

    context.user_data.clear

    show.accounts(update, context)

    return const.END


def cancel_accounting(update, context):
    update.callback_query.answer()
    show.accounts(update, context)
    return const.END
