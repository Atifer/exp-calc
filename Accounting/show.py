# module to show accounts
from telegram import (
    InlineKeyboardButton,
    InlineKeyboardMarkup,
)
from mongoConnection import user_collection
from Constants import constants as const
import sys

# float formatter
pretty_number = lambda num: num if num % 1 else int(num)


def accounts(update, context):
    try:
        if not user_collection.find_one({"_id": update.effective_user.id}):
            user_collection.insert_one(
                {
                    "_id": update.effective_user.id,
                    "username": update.effective_user.username,
                    "name": update.effective_user.full_name,
                    "accounts": {},
                }
            )
    except:
        print("Unexpected error on new user saving:", sys.exc_info()[0])

    user_accounts = user_collection.find_one(update.effective_user.id).get("accounts")

    if user_accounts:#if the user has custom accounts
        account_management_buttons = [
            [InlineKeyboardButton("اضافه کردن حساب", callback_data="addAccount")],
            [
                InlineKeyboardButton(
                    "بازگشت به حالت پیشفرض", callback_data="deleteAccounts"
                )
            ],
            [InlineKeyboardButton("تغییر حساب‌ها", callback_data="editAccount")],
        ]
        account_management_markup = InlineKeyboardMarkup(account_management_buttons)

        message = "حساب‌های شما:"
        for i in user_accounts:
            message += (
                "\n" + i + " %" + str(pretty_number(round(user_accounts[i] * 100, 2)))
            )
        message += "\n\nجمع درصدها: %" + str(
            pretty_number(round(sum(user_accounts.values()) * 100, 2))
        )

        message+=("\n\nبرای محاسبه نحوه تقسیم مبلغ دریافتی خود،"
        " آن را به عدد برای من ارسال کنید.")

        update.effective_user.send_message(
            message, reply_markup=account_management_markup
        )
    else:
        default_account_management_buttons = [
            [InlineKeyboardButton("تعریف حساب‌های شخصی", callback_data="addAccount")],
            [InlineKeyboardButton("می‌خوام حساب‌های پیشفرض مال خودم بشن!", callback_data="personalize")],
        ]
        default_account_management_markup = InlineKeyboardMarkup(
            default_account_management_buttons
        )

        message = "حساب‌های پیشفرض:"
        for i in const.DEFAULT_ACCOUNTS:
            message += "\n" + i + " %" + str(int(const.DEFAULT_ACCOUNTS[i] * 100))
        message += "\n\nجمع درصدها: %" + str(
            int(sum(const.DEFAULT_ACCOUNTS.values()) * 100)
        )
        update.effective_user.send_message(
            message, reply_markup=default_account_management_markup
        )
    
    return const.CALCULATOR


# to answer the callback query before showing accounts
def call_accounts_query(update, context):
    query = update.callback_query
    query.answer()
    accounts(update, context)

def handle_non_numeric(update, context):
    update.message.reply_text("لطفا عدد وارد نمایید.")
    return const.CALCULATOR
    #return const.CALCULATOR