# this module handles the calculator conversation with its methods
# includes these methods:
# roundmulti(lambda) , calculate_check , get_check
from telegram import (
    InlineKeyboardButton,
    InlineKeyboardMarkup,
)
from mongoConnection import user_collection
from Constants import constants as const
import sys

# returns the bottom nearest dividable to 5
roundmulti = lambda num, base=5: base * round(num / base)

# to calculate the given number that is the recieved money of the user
def calculate_check(check, accounts):
    result = "مبلغ دریافتی: " + str(check) + "\n"
    overflow = check
    for i in accounts:
        result += "\n" + i + ": " + str(roundmulti(int(check * accounts[i])))
        overflow -= roundmulti(check * accounts[i])
    if sum(accounts.values()) < 1:
        result += "\n" + "مبلغ باقیمانده: " + str(overflow)
    return result

# each message send to bot normally is treated as a check and will be calculated
def get_check(update, context):
    acc_button = [[InlineKeyboardButton("مدیریت حساب‌ها🎛", callback_data="acc")]]
    acc_inline_key = InlineKeyboardMarkup(acc_button)
    try:
        if not user_collection.find_one({"_id": update.effective_user.id}):
            user_collection.insert_one(
                {
                    "_id": update.effective_user.id,
                    "username": update.effective_user.username,
                    "name": update.effective_user.full_name,
                    "accounts": {},
                }
            )
    except:
        print("Unexpected error on new user saving:", sys.exc_info()[0])

    try:
        if float(update.message.text) < 10:
            update.message.reply_text("مبلغ خیلی کمه، نیازی به تقسیم بندی نیست.")
        else:
            user_accounts = user_collection.find_one(
                {"_id": update.effective_user.id}
            ).get("accounts", None)
            if user_accounts:
                accounts = user_accounts
            else:
                accounts = const.DEFAULT_ACCOUNTS
            message = float(update.message.text)
            message = calculate_check(message, accounts)
            update.message.reply_text(message, reply_markup=acc_inline_key)
            if accounts == const.DEFAULT_ACCOUNTS:
                update.message.reply_text(
                    "شما از حساب‌های پیشفرض من استفاده کردید، "
                    'اگر می‌خواید می‌تونید حساب‌های دلخواهتون رو با استفاده از کلید "مدیریت حساب‌ها🎛" تعریف کنید.'
                )
    except ValueError:
        update.message.reply_text("لطفا یک عدد وارد کنید!")
    except OverflowError:
        pass

def calc_stop():
    return const.END