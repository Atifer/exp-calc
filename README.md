# Expense Claculation Telegram Bot

A tool for the lazy! If you have a lot of bank accounts, each for a unique purpose, this telegram bot helps you to calculate the money you want to keep in each of your accounts.

## Getting Started

**Fork**, **Clone**, **Run**!
The main file is 'expCalcBot.py'

### Prerequisites

MongoDB
python-telegram-bot python package
A telegram API token

## Deployment

If you want to deploy the bot on a server, first make sure to read the liscense then convert the main file into a service and run the service on the server and you are good to go.

## Built With

* [python-telegram-bot](https://github.com/python-telegram-bot)

## License

This project is licensed under the GNU License - see the [LICENSE.md](LICENSE.md) file for details
