# module to handle start command of the bot

from telegram import (
    InlineKeyboardButton,
    InlineKeyboardMarkup,
)
from Constants import constants as const
from mongoConnection import user_collection
import sys

# showing some help and start the bot
def start(update, context):
    try:
        if not user_collection.find_one({"_id": update.effective_user.id}):
            user_collection.insert_one(
                {
                    "_id": update.effective_user.id,
                    "username": update.effective_user.username,
                    "name": update.effective_user.full_name,
                    "accounts": {},
                }
            )
    except:
        print("Unexpected error on new user saving:", sys.exc_info()[0])

    # acc_button = [[InlineKeyboardButton()]]
    # start_inlineKeyboard = InlineKeyboardMarkup(acc_button)
    update.message.reply_text(
        "سلام! من یه رباتم که کارای شمارو راحت تر می‌کنه"
        ".\nمن می‌تونم مبلغ دریافتی شمارو بگیرم و تو حسابای مختلف تقسیمش کنم."
        " پس برای شروع مبلغ دریافتی رو به صورت عدد برای من بفرستید.",
    )

    return const.CALCULATOR