import pymongo
import settings
# Mongo Connection
mongoClient = pymongo.MongoClient(settings.MONGO)
db = mongoClient["expBotDB"]
user_collection = db["user"]